//#[macro_use] extern crate maplit;
use path_absolutize::*;
use colored::*;
use std::collections::HashMap;
use std::io::Write;
use std::process::{Command, Stdio};
use std::sync::mpsc::{TryRecvError, channel};
//use ctrlc;
//use std::thread;
//use walkdir::WalkDir;
//use path_absolutize::Absolutize;
use std::env;
use std::path::{Path, PathBuf};
use std::fs::OpenOptions;
//use argparse::{ArgumentParser, Store, StoreTrue};
use structopt::StructOpt;

/// Tests APKBUILDS
#[derive(StructOpt, Debug, Clone)]
#[structopt(name = "abuildtester")]
struct ABuildTesterConfig {
    /// Output file.  Defaults to ./results.txt
    #[structopt(short, long, parse(from_os_str))]
    output: Option<PathBuf>,

    /// Type of test to run.  Use list here for a list of valid tests
    #[structopt(name = "TEST_TYPE")]
    test_type: String,

    /// List of apkbuild directories to check
    #[structopt(long="directory-list", parse(from_os_str))]
    dir_list: Option<Vec<PathBuf>>,

    /// Number of times to retry downloading if a fetch fails
    #[structopt(long="download-retries", default_value="3")]
    download_retries: u8,

    /// Include reason for fail in output
    #[structopt(long="include-reason")]
    include_reason: bool,

    /// Process only packages that depend on this
    #[structopt(short, long)]
    depends_on: Option<String>,
}

#[derive(Hash, Clone, Debug)]
struct TestInfo {
    args: Option<Vec<String>>,
    help: String,
}

impl TestInfo {
    pub fn new(args: Vec<impl Into<String>>, help: impl Into<String>) -> Self {
        if args.len() > 0 {
            let mut arg_list: Vec<String> = Vec::new();
            for arg in args {
                arg_list.push(arg.into());
            }
            return Self {
                args: Some(arg_list),
                help: help.into(),
            }
        } else {
            return Self {
                args: None,
                help: help.into(),
            }
        }
    }
}

fn main() {
    const SOURCES_BASH_FILE: &str = ". ./APKBUILD\necho $source\n";
    const DEPENDS_BASH_FILE: &str = ". ./APKBUILD\necho $depends\n";
    const PKGNAME_BASH_FILE: &str = ". ./APKBUILD\necho $pkgname\n";
    //const MAKEDEPENDS_BASH_FILE: &str = ". ./APKBUILD\necho $makedepends\n";
    let mut test_info: HashMap<&str, TestInfo> = HashMap::new();
    test_info.insert("fetch", TestInfo::new(vec!["fetch"], "Tests fetching sources"));
    test_info.insert("build", TestInfo::new(vec!["-R"], "Tests abuild -R"));
    test_info.insert("list", TestInfo::new(Vec::<String>::new(), "Lists test types and what they do"));
    //let valid_tests = vec!["fetch", "build", "list"];
    let (tx, rx) = channel();
    let tx_clone = tx.clone();
        ctrlc::set_handler(move || tx_clone.send("ctrlc").expect("Could not send signal on channel."))
            .expect("Error setting Ctrl-C handler");
    
    let opts = ABuildTesterConfig::from_args();
    let opts_num_retries = opts.clone().download_retries;
    let orig_dir = env::current_dir().unwrap();
    let include_reason = opts.clone().include_reason;
    //let mut output_path;
    let opt_output = opts.clone().output;
    let output_path = match opt_output {
        Some(x) => x.absolutize().unwrap().into_owned(),
        None => {
            let mut y = env::current_dir().unwrap();
            y.push("results.txt");
            y
        }
    };
    //let mut test_type: String;
    let test_type = opts.test_type.as_str();
    //let mut output_path = env::current_dir().unwrap();
    
    //let mut build = false;
    //output_path.push("results.txt");
    /*
    {
        let mut ap = ArgumentParser::new();
        ap.set_description("Tests APKBUILDs");
        ap.refer(&mut output_path)
            .add_option(&["-o", "--output"], Store, "Path to results file.  Default is ./results.txt");
        ap.refer(&mut build)
            .add_option(&["-b", "--build"], StoreTrue, "Build instead of just fetching");
        ap.parse_args_or_exit();
    }
    */
    let mut completed_packages: Vec<String> = Vec::new();
    if !test_info.contains_key(&test_type) {
        eprintln!("Invalid test type.  Use the list test type for a list of valid test types");
        std::process::exit(exitcode::USAGE);
    }
    if test_type == "list" {
        println!("List of test types and what they do:");
        for info in test_info.keys() {
            println!("{} => {}", info, test_info.get(info).unwrap().help);
        }
        std::process::exit(exitcode::OK);
    }
    let cmd_args = test_info.get(test_type).unwrap().args.as_ref().unwrap().into_iter().map(|x| x.as_str()).collect::<Vec<&str>>();
    /*
    if test_type == "build" {
        cmd_arg = "-R";
    } else if test_type == "fetch" {
        cmd_arg = "fetch";
    } else {
        cmd_arg = "error";
    }
    */
    let mut depends_on_vec: Vec<String> = Vec::new();
    let mut depends_hashmap: HashMap<String, Vec<String>> = HashMap::new();
    let mut final_depends: Vec<String> = Vec::new();
    if output_path.exists() {
        for (num, line) in std::fs::read_to_string(output_path.clone()).unwrap().replace("\r", "").split("\n").enumerate() {
            if line.trim() != "" {
                let parts = line.split_once(": ").expect(format!("Error parsing at line {}", num + 1).as_str());
                completed_packages.push(parts.0.to_string())
            }
        }
    }
    let dir_entries = std::fs::read_dir(Path::new("./").absolutize().unwrap().into_owned()).expect("Error reading current directory");
    let filter_directories = dir_entries.filter(|x| x.as_ref().unwrap().path().is_dir());
    let mut directories = filter_directories.map(|x| x.as_ref().unwrap().path()).collect::<Vec<PathBuf>>();
    if opts.clone().dir_list.is_some() {
        directories = opts.clone().dir_list.unwrap();
        directories = directories.into_iter().filter(|x| x.is_dir()).collect::<Vec<PathBuf>>();
    }
    if opts.clone().depends_on.is_some() {
        for directory in directories.clone() {
            env::set_current_dir(directory.clone()).expect(format!("Unable to change directory to {}", directory.clone().display()).as_str());
            {
                let mut sh_file = std::fs::File::create("TMP_SH.sh").unwrap();
                sh_file.write_all(DEPENDS_BASH_FILE.as_bytes()).unwrap();
            }
            let cmd_output = Command::new("sh").arg("./TMP_SH.sh").output().unwrap();
            let depends_data = String::from_utf8_lossy(cmd_output.stdout.as_slice());
            std::fs::remove_file("./TMP_SH.sh").expect("Could not remove ./TMP_SH.sh");
            let depends: Vec<String> = depends_data.split_whitespace().into_iter().map(|x| x.to_string()).collect();
            /*
            {
                let mut sh_file = std::fs::File::create("TMP_SH.sh").unwrap();
                sh_file.write_all(MAKEDEPENDS_BASH_FILE.as_bytes()).unwrap();
            }
            let cmd_output = Command::new("sh").arg("./TMP_SH.sh").output().unwrap();
            let make_depends_data = String::from_utf8_lossy(cmd_output.stdout.as_slice());
            std::fs::remove_file("./TMP_SH.sh").expect("Could not remove ./TMP_SH.sh");
            let make_depends: Vec<String> = depends_data.split_whitespace().into_iter().map(|x| x.to_string()).collect();
            */
            {
                let mut sh_file = std::fs::File::create("TMP_SH.sh").unwrap();
                sh_file.write_all(PKGNAME_BASH_FILE.as_bytes()).unwrap();
            }
            let cmd_output = Command::new("sh").arg("./TMP_SH.sh").output().unwrap();
            let package_name = String::from_utf8_lossy(cmd_output.stdout.as_slice()).trim().to_string();
            std::fs::remove_file("./TMP_SH.sh").expect("Could not remove ./TMP_SH.sh");
            if !depends.contains(&opts.clone().depends_on.unwrap()) {
                depends_hashmap.insert(package_name, depends);
            } else {
                depends_on_vec.push(package_name);
            }
        }
        while !depends_on_vec.is_empty() {
            for depend in depends_on_vec.clone() {
                for key in depends_hashmap.keys() {
                    if depends_hashmap.get(key).unwrap().contains(&depend) {
                        if !final_depends.contains(&depend) {
                            depends_on_vec.push(depend.to_string());
                        }
                    }
                }
                final_depends.push(depend.clone());
                let depend_clone = depend.clone();
                depends_on_vec.retain(|x| *x != depend_clone);
            }
        }
        for directory in directories.clone() {
            if !final_depends.contains(&directory.file_name().unwrap().to_str().unwrap().to_string()) {
                directories.retain(|x| *x != directory);
            }
        }
        println!("Packages that depend on {} are: {}", opts.clone().depends_on.unwrap(), final_depends.join(", "));
    }
    //println!("{:?}", directories.map(|x| x.as_ref().unwrap().path().display().to_string()).collect::<Vec<String>>())
    for directory in directories {
        //let path = directory.unwrap().path();
        let path = directory.clone();
        if !completed_packages.contains(&path.file_name().unwrap().to_str().unwrap().to_string()) {
            env::set_current_dir(path.clone()).expect(format!("Unable to change directory to {}", path.clone().display()).as_str());
            
            //let mut works = false;
            if Path::new("./APKBUILD").exists() {
                let mut out_string: String;
                if test_type != "fetch" {
                    let mut child = Command::new("abuild").args(&cmd_args).stdout(Stdio::inherit()).stderr(Stdio::inherit()).stdin(Stdio::inherit()).spawn().expect(format!("Error spawning abuild -R for package '{}'", path.file_name().unwrap().to_str().unwrap()).as_str());
                    let child_id = child.id() as i32;
                    let (tx2, rx2) = channel();
                    let tx_clone2 = tx.clone();
                    let tx2_clone = tx2.clone();
                    std::thread::spawn(move || {
                        tx2_clone.send(child.wait().expect("abuild not running").success()).unwrap();
                        tx_clone2.send("done").unwrap();
                    });
                    let mut status = match rx.try_recv() {
                        Ok(rx) => rx,
                        Err(TryRecvError::Empty) => "empty",
                        Err(TryRecvError::Disconnected) => "disconnected", 
                    };
                    while status != "ctrlc" && status != "done" {
                        status = match rx.try_recv() {
                            Ok(rx) => rx,
                            Err(TryRecvError::Empty) => "empty",
                            Err(TryRecvError::Disconnected) => "disconnected",
                        };
                        std::thread::sleep(std::time::Duration::from_micros(1));
                    }
                    if status == "ctrlc" {
                        //tx2.send("quit").unwrap();
                        nix::sys::signal::kill(
                            nix::unistd::Pid::from_raw(child_id), 
                            nix::sys::signal::Signal::SIGINT
                        ).expect("cannot send ctrl-c");
                        return
                    }

                    //let status = rx2.recv().unwrap();
                    //if status == "quit" {
                    //    return;
                    //}
                    
                    
                    let works = rx2.recv().unwrap();
                    if works {
                        out_string = "PASS".to_string();
                    } else {
                        out_string = "FAIL".to_string();
                    }
                } else {
                    let (tx2, rx2) = channel();
                    let tx_clone2 = tx.clone();
                    let tx2_clone = tx2.clone();
                    std::thread::spawn(move || {
                        {
                            let mut sh_file = std::fs::File::create("TMP_SH.sh").unwrap();
                            sh_file.write_all(SOURCES_BASH_FILE.as_bytes()).unwrap();
                        }
                        let cmd_output = Command::new("sh").arg("./TMP_SH.sh").output().unwrap();
                        let source = String::from_utf8_lossy(cmd_output.stdout.as_slice());
                        std::fs::remove_file("./TMP_SH.sh").expect("Could not remove ./TMP_SH.sh");
                        let sources = source.split_whitespace();
                        //let mut success = true;
                        let mut success_reason: (bool, Option<String>) = (true, None);
                        //tx2_clone.send(child.wait().expect("abuild not running").success()).unwrap();
                        for src in sources {
                            let mut num_retries = opts_num_retries;
                            //let url: String;
                            if src.contains("::") && src.contains("//") {
                                println!("Downloading {}", src.split_once("::").unwrap().1);
                                //let mut success_reason: (bool, Option<String>); 
                                success_reason = match abuildtester::buffered_download(src.split_once("::").unwrap().1) {
                                    Ok(x) => x,
                                    Err(y) => (false, Some(y.to_string())),
                                };
                                while !success_reason.0 && num_retries != 0 {
                                    success_reason = match abuildtester::buffered_download(src.split_once("::").unwrap().1) {
                                        Ok(x) => x,
                                        Err(y) => {
                                            println!("Error downloading {}. Retry {}", src.split_once("::").unwrap().1, num_retries);
                                            (false, Some(y.to_string()))
                                        },
                                    };
                                    num_retries -= 1;
                                }
                            } else if src.contains("//") && !src.contains("::") {
                                //success = abuildtester::buffered_download(src).is_ok();
                                //let mut success_reason: (bool, Option<String>);
                                println!("Downloading {}", src);
                                success_reason = match abuildtester::buffered_download(src) {
                                    Ok(x) => x,
                                    Err(y) => (false, Some(y.to_string())),
                                };
                                while !success_reason.0 && num_retries != 0 {
                                    success_reason = match abuildtester::buffered_download(src) {
                                        Ok(x) => x,
                                        Err(y) => {
                                            println!("Error downloading {}. Retry {}", src, num_retries);
                                            (false, Some(y.to_string()))
                                        },
                                    };
                                    num_retries -= 1;
                                }
                            } else {
                                success_reason = (true, None);
                            }
                            if !success_reason.0 {
                                break;
                            }
                        }
                        tx2_clone.send(success_reason).unwrap();
                        tx_clone2.send("done").unwrap();
                    });
                    let mut status = match rx.try_recv() {
                        Ok(rx) => rx,
                        Err(TryRecvError::Empty) => "empty",
                        Err(TryRecvError::Disconnected) => "disconnected", 
                    };
                    while status != "ctrlc" && status != "done" {
                        status = match rx.try_recv() {
                            Ok(rx) => rx,
                            Err(TryRecvError::Empty) => "empty",
                            Err(TryRecvError::Disconnected) => "disconnected",
                        };
                        std::thread::sleep(std::time::Duration::from_micros(1));
                    }
                    if status == "ctrlc" {
                        //tx2.send("quit").unwrap();
                        //nix::sys::signal::kill(
                        //    nix::unistd::Pid::from_raw(child_id), 
                        //    nix::sys::signal::Signal::SIGINT
                        //).expect("cannot send ctrl-c");
                        return
                    }

                    //let status = rx2.recv().unwrap();
                    //if status == "quit" {
                    //    return;
                    //}
                    
                    
                    let works = rx2.recv().unwrap();
                    let mut reason = String::new();
                    if include_reason && works.1.is_some() {
                        reason = works.1.unwrap();
                    }
                    if works.0 {
                        out_string = "PASS".to_string();
                    } else {
                        out_string = format!("FAIL -> {}", reason);
                    }
                }
                {
                    let mut out_file = OpenOptions::new().append(true).create(true).open(output_path.clone()).unwrap();
                    out_file.write_all(format!("{}: {}\n", path.file_name().unwrap().to_str().unwrap(), &out_string).as_bytes()).expect("Unable to write to results file.");
                    if out_string.as_str() == "PASS" {
                        out_string = out_string.as_str().bright_green().to_string();
                    } else {
                        out_string = out_string.as_str().bright_red().to_string();
                    }
                    println!("{}: {}", path.file_name().unwrap().to_str().unwrap(), &out_string)
                }
            }
            env::set_current_dir(orig_dir.clone()).unwrap();
        }
    }
}
