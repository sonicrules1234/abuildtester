use std::io::BufReader;
use std::io::Read;

pub fn buffered_download(url: impl Into<String>) -> Result<(bool, Option<String>), ureq::Error> {
    let url_string = url.into();
    let response = ureq::get(url_string.as_str()).call()?;
    if response.content_type().trim() == "text/html" && (!url_string.ends_with(".html") || !url_string.ends_with(".htm") || !url_string.ends_with(".xhtml")) {
        return Ok((false, Some("Content-type was 'text/html' on wrong type of file".to_string())));
    }
    //let status_code = response.status();
    //let status_text = response.status_text();
    //if status_code != 200 {
    //
    //}
    let mut reader = BufReader::new(response.into_reader());
    //let mut buf: Vec<u8> = Vec::new();
    //let mut chunk = reader.take(4096);
    /*
    while match chunk.read_to_end(&mut buf) {
        Ok(_) => {
            //buf = Vec::new();
            true
        }
        Err(_) => false,
    } {
        
        if buf.is_empty() {
            return Ok(true);
        } else {
            chunk = reader.take(4096);
        }
    }
    return Ok(buf.is_empty());
    */
    
    //let buf: Vec<u8>;
    /*
    let mut keepgoing = match buf_result {
        Ok(ref x) => {
            buf = x.to_vec();
            if buf.is_empty() {
                return Ok(true)
            }
            true
        },
        Err(_) => false, 
    };
    */
    let mut keep_going = true;
    let mut reason: Option<String> = None;
    while keep_going {
        let buf_result = read_bytes(&mut reader);
        keep_going = match buf_result {
            Ok(ref x) => {
                //buf = x.to_vec();
                if x.is_empty() {
                    return Ok((true, None))
                }
                true
            },
            Err(x) => {
                reason = Some(x.to_string());
                false
            }, 
        };
    }
    return Ok((false, reason));
}
fn read_bytes(reader: &mut (impl Read + Send)) -> Result<Vec<u8>, std::io::Error> {
    let mut chunk = reader.take(409600000);
    let mut buf: Vec<u8> = Vec::new();
    chunk.read_to_end(&mut buf)?;
    Ok(buf)
}